//
//  ProducterStream.h
//  d00
//
//  Created by Jean Gravier on 15/09/2014.
//  Copyright (c) 2014 Jean Gravier. All rights reserved.
//

#ifndef __d00__ProducterStream__
#define __d00__ProducterStream__

#include <iostream>
#include <fstream>

#define BLOCK_SIZE 32

class ProducterStream {
public:
    ProducterStream();
    ~ProducterStream();
    
    std::string	nextString();
    bool		loadFile(char *path);
    bool		loadStdin();
    
private:
    std::ifstream	_stream;
};

#endif /* defined(__d00__ProducterStream__) */
