//
//  ProducterStream.cpp
//  d00
//
//  Created by Jean Gravier on 15/09/2014.
//  Copyright (c) 2014 Jean Gravier. All rights reserved.
//

#include <ios>
#include <cstring>
#include "ProducterStream.hpp"

ProducterStream::ProducterStream()
{

}

ProducterStream::~ProducterStream()
{
    _stream.close();
}

std::string	ProducterStream::nextString()
{
    char	buffer[BLOCK_SIZE + 1];

    std::memset(buffer, 0, BLOCK_SIZE + 1);

    if (_stream.fail())
        throw std::exception();

    _stream.read(buffer, BLOCK_SIZE);
    buffer[BLOCK_SIZE] = '\0';

    return (std::string(buffer));
}

bool		ProducterStream::loadFile(char *path)
{
    if (_stream.is_open())
        _stream.close();

    _stream.open(path);

    return (_stream.good());
}

bool		ProducterStream::loadStdin()
{
    _stream.std::ios::rdbuf(std::cin.rdbuf());

    return (_stream.good());
}
